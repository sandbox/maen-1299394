/********************************************************
*Overview and sense
*********************************************************/
In Germany we have 2 different vat levels: 7% and 19%.
In Ubercart there's the possibility to create multiple classes to give them 
different taxes. But we found that it's not a very flexible system. So we 
solved it by building a new module called uc_vat_with_taxonomy.
The result:
Inspired by leon.nk who build it generally for taxes it's now possible to 
change taxes on product level. You don't have to create different classes 
for different taxes now.
You can use this module for mass-imports with the module "node-import". 
/********************************************************
*Installation
*********************************************************/
Install the module unter sites/all/modules as always. Uninstall uc_vat if 
installed. Remove the db-tables of uc_vat! It's important! 
In Taxonomy either create a new taxonomy or use "catalog". I prefer a 
vocabulary named "taxes". Be sure to enable the vocabulary for the articles 
you need it. Create some self-explaining terms like "Vat 19%" or "VAT 7%" or 
"MwST 19%" or anything like these.
Go to ...admin/store/settings/taxes/vat, do whatever you want to but let the 
vendor input his sales prices as net prices without VAT.
Than create a tax, name it whatever you want, for easyness I would prefer to 
give an appropriate name like them above ...
Activate the term you want, product_type doesn't play a role.
Create a node, combine it with the term you want, save. Go to cart and enjoy...



