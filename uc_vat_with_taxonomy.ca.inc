<?php
/**
 * Implementation of hook_ca_condition().
 */
function uc_vat_with_taxonomy_ca_condition() {
  $conditions['uc_vat_with_taxonomy_condition_delivery_country_eu'] = array(
    '#title' => t("Order shipping country is inside the EU"),
    '#category' => t('Order: Shipping address'),
    '#description' => t('Returns TRUE if the shipping country is inside the European Union.'),
    '#callback' => 'uc_vat_with_taxonomy_condition_delivery_country_eu',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
    ),
  );
  $conditions['uc_vat_with_taxonomy_condition_billing_country_eu'] = array(
    '#title' => t("Order billing country is inside the EU"),
    '#category' => t('Order: Billing address'),
    '#description' => t('Returns TRUE if the billing country is inside the European Union.'),
    '#callback' => 'uc_vat_with_taxonomy_condition_billing_country_eu',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
    ),
  );
  return $conditions;
}

/**
 * Check an order's delivery country is inside the EU.
 */
function uc_vat_with_taxonomy_condition_delivery_country_eu($order, $settings) {
  return in_array($order->delivery_country, uc_vat_with_taxonomy_eu_countries());
}

/**
 * Check an order's billing country is inside the EU.
 */
function uc_vat_with_taxonomy_condition_billing_country_eu($order, $settings) {
  return in_array($order->billing_country, uc_vat_with_taxonomy_eu_countries());
}

/**
 * Return an array of ISO 3166-1 numeric codes of the 27 EU countries.
 */
function uc_vat_with_taxonomy_eu_countries() {
  return array(
// Austria
          40,  
// Belgium
          56,  
// Bulgaria
          100, 
// Cyprus
          196, 
// Czech Republic
          203, 
// Denmark
          208, 
// Estonia
          233, 
// Finland
          246, 
// France
          250, 
// Germany
          276, 
// Greece
          300, 
// Hungary
          348, 
// Ireland
          372, 
// Italy
          380, 
// Latvia
          428, 
// Lithuania
          440, 
// Luxembourg
          442, 
// Malta
          470, 
// Netherlands
          528, 
// Poland
          616, 
// Portugal
          620, 
// Romania
          642, 
// Slovakia
          703, 
// Slovenia
          705, 
// Spain
          724, 
// Sweden
          752, 
// United Kingdom
          826, 
  );
}

